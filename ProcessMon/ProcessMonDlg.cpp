
// ProcessMonDlg.cpp : 구현 파일
//

#include "stdafx.h"
#include "ProcessMon.h"
#include "ProcessMonDlg.h"
#include "afxdialogex.h"
#include <Tlhelp32.h>




#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// 응용 프로그램 정보에 사용되는 CAboutDlg 대화 상자입니다.

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

// 구현입니다.
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// CProcessMonDlg 대화 상자




CProcessMonDlg::CProcessMonDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CProcessMonDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CProcessMonDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST_PROCESS, m_ListProcess);
	DDX_Control(pDX, IDC_STATIC_PROCESS1, m_staticProcess);
}

BEGIN_MESSAGE_MAP(CProcessMonDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_WM_TIMER()
	ON_MESSAGE(WM_TRAYICON_MSG, TrayIconMessage)
	ON_WM_DESTROY()
END_MESSAGE_MAP()


// CProcessMonDlg 메시지 처리기

BOOL CProcessMonDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// 시스템 메뉴에 "정보..." 메뉴 항목을 추가합니다.

	// IDM_ABOUTBOX는 시스템 명령 범위에 있어야 합니다.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 이 대화 상자의 아이콘을 설정합니다. 응용 프로그램의 주 창이 대화 상자가 아닐 경우에는
	//  프레임워크가 이 작업을 자동으로 수행합니다.
	SetIcon(m_hIcon, TRUE);			// 큰 아이콘을 설정합니다.
	SetIcon(m_hIcon, FALSE);		// 작은 아이콘을 설정합니다.

	ShowWindow(SW_NORMAL);
	m_WinStatus = SW_NORMAL;

	// TODO: 여기에 추가 초기화 작업을 추가합니다.

	m_font.CreateFont(24,0,   // nHeight, nWidth
     0,                         // nEscapement
     0,           // nOrientation              
     FW_BOLD,   // nWeight              
     FALSE,       // bItalic              
     FALSE,       // bUnderline              
     0,           // cStrikeOut              
     ANSI_CHARSET,// nCharSet              
     OUT_DEFAULT_PRECIS,        // nOutPrecision
     CLIP_DEFAULT_PRECIS,       // nClipPrecision
     DEFAULT_QUALITY,           // nQuality
     DEFAULT_PITCH | FF_SWISS,  // nPitchAndFamily
     _T("굴림")); 

	m_staticProcess.SetFont(&m_font);


//	GetProcessList();

	CoInitialize(NULL);

	CString iniName;
	CString strValue;
	TCHAR pszPathName[_MAX_PATH]; 
	GetModuleFileName(::AfxGetInstanceHandle(), pszPathName, _MAX_PATH); 
	iniName.Format("%s", pszPathName);

	int brk = 0;
	brk = iniName.ReverseFind('\\');
	if(brk)
		iniName = iniName.Left(brk);
	iniName = iniName + "\\CCUConfig.ini";

	m_trackType = GetPrivateProfileInt("PROCESS_MONITOR", "TRACKING_MONITOR", 0, iniName);

	if (m_trackType == 2)
	{
		m_exeIdx = 1;
		m_hr = m_CapDev.OpenVideo(320, 240, 1, 0, 0);
	}

	//2017.12.28 SIJ
	if( (m_trackType < 0) || (m_trackType > 2) )
		m_trackType = 0;

	m_exeIdx = 0;
	m_execnt = 40;

	m_exeName[0] = _T("RadarCam.exe");

	if(m_trackType == 2)
	{
		m_exeName[1] = STRING_RTTRACK_NAME;
		m_trackPath = STRING_RTTRACK_PATH;
	}

	m_bTrayStatus = false;
	TraySetting();

	m_hr = -1;

	SetTimer(WM_TIMER_GET_PROCESS_LIST, 500, NULL);

	Sleep(50);
	if (m_trackType == 2)
	{
		if (SUCCEEDED(m_hr))
			m_CapDev.CloseVideo();
	}

	return TRUE;  // 포커스를 컨트롤에 설정하지 않으면 TRUE를 반환합니다.
}


void CProcessMonDlg::TraySetting(void)
{
	NOTIFYICONDATA nid;
	nid.cbSize = sizeof(nid);
	nid.hWnd = m_hWnd; // 메인 윈도우 핸들
	nid.uID = IDR_MAINFRAME;						// 아이콘 리소스 ID
	nid.uFlags = NIF_MESSAGE | NIF_ICON | NIF_TIP;	// 플래그 설정
	nid.uCallbackMessage = WM_TRAYICON_MSG;			// 콜백메시지 설정
	nid.hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME); // 아이콘 로드

	char strTitle[256];
	GetWindowText(strTitle, sizeof(strTitle)); // 캡션바에 출력된 문자열 얻음
	lstrcpy(nid.szTip, strTitle);
	Shell_NotifyIcon(NIM_ADD, &nid);
	SendMessage(WM_SETICON, (WPARAM)TRUE, (LPARAM)nid.hIcon);
	m_bTrayStatus = true;
}


LRESULT CProcessMonDlg::TrayIconMessage(WPARAM wParam, LPARAM lParam)
{
	// 등록된 트레이 아이콘을 클릭하면 다이얼로그를 볼수있게 한다.
	if(lParam == WM_LBUTTONDBLCLK) { 
		ShowWindow(SW_SHOW);
		m_WinStatus = SW_SHOW;
	}
	return 0L;
}


void CProcessMonDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else if( nID == SC_MINIMIZE )
	{
		ShowWindow(SW_HIDE);
		m_WinStatus = SW_HIDE;
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// 대화 상자에 최소화 단추를 추가할 경우 아이콘을 그리려면
//  아래 코드가 필요합니다. 문서/뷰 모델을 사용하는 MFC 응용 프로그램의 경우에는
//  프레임워크에서 이 작업을 자동으로 수행합니다.

void CProcessMonDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 그리기를 위한 디바이스 컨텍스트입니다.

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 클라이언트 사각형에서 아이콘을 가운데에 맞춥니다.
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 아이콘을 그립니다.
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// 사용자가 최소화된 창을 끄는 동안에 커서가 표시되도록 시스템에서
//  이 함수를 호출합니다.
HCURSOR CProcessMonDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}


#if 0
void CProcessMonDlg::GetProcessList(void)
{
	PROCESSENTRY32 Process32;
	HANDLE SHandle;
	BOOL bNext;
  
	Process32.dwSize = sizeof(PROCESSENTRY32);
	SHandle = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);

	m_ListProcess.ResetContent();

	// 프로세스 리스트를 리스트뷰에 추가
	if(Process32First(SHandle, &Process32))
	{
		bNext = true;
		while(bNext)
		{
			m_ListProcess.AddString(Process32.szExeFile);
			bNext = Process32Next(SHandle, &Process32);
		}

      /*if String(Process32.szExeFile) = 'runEnfCam.exe' then
        item := ListView_ProcessList.Items.Insert(0)
      else if String(Process32.szExeFile) = 'RadarCam.exe' then
        item := ListView_ProcessList.Items.Insert(0)
      else
item := ListView_ProcessList.Items.Add;*/

//		m_ListProcess.AddString(Process32.szExeFile);
//		item.SubItems.Add(IntToStr(Process32.th32ProcessID));
	}

  CloseHandle(SHandle);
}
#endif


BOOL CALLBACK EnumWindowsProc(  HWND hWnd, LPARAM lParam )
{
	DWORD PID = 0;
	::GetWindowThreadProcessId(hWnd, &PID);
 
	if((DWORD)lParam == PID)
	{
          SendMessage(hWnd, WM_CLOSE, 0, 0);
	}
	return TRUE;
}


bool CProcessMonDlg::IsProcessExists(LPCTSTR exeName, bool bKill)
{
	PROCESSENTRY32 Process32;
	HANDLE   SHandle;
	BOOL     bNext;
	CString  str;

  
	Process32.dwSize = sizeof(PROCESSENTRY32);
	SHandle = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);

	if(SHandle == INVALID_HANDLE_VALUE)
		return false;

	// 프로세스 리스트를 리스트뷰에 추가
	if(Process32First(SHandle, &Process32))
	{
		bNext = true;
		while(bNext)
		{
			str = Process32.szExeFile;

			if(str.CompareNoCase(exeName) == 0)
			{
				if(bKill == true)
				{
					EnumWindows(EnumWindowsProc, Process32.th32ProcessID);
				}

				CloseHandle(SHandle);
				return true;
			}

			bNext = Process32Next(SHandle, &Process32);
		}
	}

  CloseHandle(SHandle);
  return false;
}


void CProcessMonDlg::executeMainSW(void)
{
	char filename[256];
	TCHAR    tzPath[1024];


	if(IsProcessExists("runEnfCam.exe", true) == TRUE)
	{
		return;
	}


	//2022.1.08
	LPCTSTR newFileName1 = _T("D:\\JWIS\\Update\\RadarCam.exe");
	LPCTSTR oldFileName1 = _T("D:\\JWIS\\RadarCam.exe");

	LPCTSTR newFileName2 = _T("D:\\JWIS\\Update\\runEnfCam.exe");
	LPCTSTR oldFileName2 = _T("D:\\JWIS\\runEnfCam.exe");

	int retval;
	retval = PathFileExists(newFileName1);
	if (retval == TRUE)
	{
		if (PathFileExists(oldFileName1))
			DeleteFile(oldFileName1);
		Sleep(1);
		MoveFile(newFileName1, oldFileName1);
	}

	retval = PathFileExists(newFileName2);
	if (retval == TRUE)
	{
		if (PathFileExists(oldFileName2))
			DeleteFile(oldFileName2);
		Sleep(1);
		MoveFile(newFileName2, oldFileName2);
	}

	Sleep(10);

	memset(&m_mainInfo, 0, sizeof(SHELLEXECUTEINFO));
	memset(&tzPath, 0x00, sizeof(tzPath));
	DWORD dwResult = GetModuleFileName( NULL,  tzPath,  sizeof(tzPath) );
	PathRemoveFileSpec(tzPath);

	sprintf_s(filename, 256, "RadarCam.exe");

	m_mainInfo.lpDirectory = tzPath;
	m_mainInfo.hwnd = NULL;
    m_mainInfo.lpParameters = "";
    m_mainInfo.cbSize = sizeof( SHELLEXECUTEINFO );
    m_mainInfo.lpVerb = "open";
    m_mainInfo.lpFile = filename;
    m_mainInfo.nShow = SW_SHOWNORMAL;
    m_mainInfo.fMask = SEE_MASK_NOCLOSEPROCESS;

	ShellExecuteEx(&m_mainInfo);
}


void CProcessMonDlg::executeTrackSW(void)
{
	m_mainInfo.lpDirectory = m_trackPath;
	m_mainInfo.hwnd = NULL;
    m_mainInfo.lpParameters = "";
    m_mainInfo.cbSize = sizeof( SHELLEXECUTEINFO );
    m_mainInfo.lpVerb = "open";
    m_mainInfo.lpFile = m_exeName[1];
    m_mainInfo.nShow = SW_SHOWNORMAL;
    m_mainInfo.fMask = SEE_MASK_NOCLOSEPROCESS;

	ShellExecuteEx(&m_mainInfo);
}


void CProcessMonDlg::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	CString str;

	KillTimer(nIDEvent);

	switch(nIDEvent)
	{
	case WM_TIMER_GET_PROCESS_LIST :
		m_execnt--;
		
		if(IsProcessExists(m_exeName[m_exeIdx], false) == TRUE)
		{
			m_staticProcess.SetWindowText("");
			m_execnt = PROCESS_EXECUTE_INIT_COUNT;
			if( (m_exeIdx == 0) && (m_trackType == 2) )
			{
				m_exeIdx = 1;
			}
			else
				m_exeIdx = 0;

			if(m_WinStatus != SW_HIDE)
			{
				ShowWindow(SW_HIDE);
				m_WinStatus = SW_HIDE;
			}

			SetTimer(WM_TIMER_GET_PROCESS_LIST, 500, NULL);
			return;
		}

		if(m_execnt <= 0)
		{
			m_execnt = PROCESS_EXECUTE_INIT_COUNT;
			if(m_exeIdx == 0)
			{
				executeMainSW();
			}
			else
			{
				executeTrackSW();
				m_exeIdx = 0;
			}
			SetTimer(WM_TIMER_GET_PROCESS_LIST, 500, NULL);
			return;
		}

		if( (m_execnt % 2) == 0)
		{
			if(m_WinStatus == SW_HIDE)
			{
				ShowWindow(SW_SHOW);
				m_WinStatus = SW_SHOW;
			}
			if(m_exeIdx == 0)
				str.Format("%d초후 운영S/W 시작", m_execnt / 2);
			else
				str.Format("%d초후 추적S/W 시작", m_execnt / 2);
			m_staticProcess.SetWindowText(str);
			//GetProcessList();
		}

		SetTimer(WM_TIMER_GET_PROCESS_LIST, 500, NULL);
		return;

		break;
	}

	CDialogEx::OnTimer(nIDEvent);
}


void CProcessMonDlg::OnDestroy()
{
	CDialogEx::OnDestroy();

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
	if(m_bTrayStatus) {
		NOTIFYICONDATA nid;
		nid.cbSize = sizeof(nid);
		nid.hWnd = m_hWnd; // 메인 윈도우 핸들
		nid.uID = IDR_MAINFRAME;
		// 작업 표시줄(TaskBar)의 상태 영역에 아이콘을 삭제한다.
		Shell_NotifyIcon(NIM_DELETE, &nid);
	}
}
