
// ProcessMonDlg.h : 헤더 파일
//

#pragma once
#include "afxwin.h"
#include "VideoCap.h"




#define WM_TIMER_GET_PROCESS_LIST 1
#define STRING_LSTRACK_NAME _T("LSTrack.exe")
#define STRING_LSTRACK_PATH _T("D:\\TEye\\")
#define STRING_RTTRACK_NAME _T("TrackingApp.exe")
#define STRING_RTTRACK_PATH _T("D:\\RT\\bin\\")
#define PROCESS_EXECUTE_INIT_COUNT 20
#define WM_TRAYICON_MSG WM_USER + 1


// CProcessMonDlg 대화 상자
class CProcessMonDlg : public CDialogEx
{
// 생성입니다.
public:
	CProcessMonDlg(CWnd* pParent = NULL);	// 표준 생성자입니다.

// 대화 상자 데이터입니다.
	enum { IDD = IDD_PROCESSMON_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 지원입니다.


// 구현입니다.
protected:
	HICON m_hIcon;
	bool m_bTrayStatus;
	int  m_WinStatus;

	// 생성된 메시지 맵 함수
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	void TraySetting(void);
	LRESULT TrayIconMessage(WPARAM wParam, LPARAM lParam);
	DECLARE_MESSAGE_MAP()
public:
	CListBox m_ListProcess;
	CStatic  m_staticProcess;
	CFont    m_font;
	int      m_execnt;
	int      m_exeIdx;
	int      m_trackType;
	LPCTSTR  m_exeName[2];
	LPCTSTR  m_trackPath;

	CVideoCap m_CapDev;
	HRESULT   m_hr;

//	void GetProcessList(void);
	bool IsProcessExists(LPCTSTR exeName, bool bKill);
	void executeMainSW(void);
	void executeTrackSW(void);
	SHELLEXECUTEINFO m_mainInfo;
	SHELLEXECUTEINFO m_trackInfo;
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnDestroy();
};
