// DirectVideoFrame.cpp: implementation of the CDirectVideoFrame class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "VideoCap.h"
#include "include\Mtype.h"
#include "include\streams.h"
#include "include\dshowutil.h"

#include <commctrl.h>
#include <commdlg.h>
#include <stdio.h>
#include <tchar.h>

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

#ifdef DEBUG
#define REGISTER_FILTERGRAPH
#endif

unsigned long captureIndex;

/***********************************************************88*/
//				Callback function
/***********************************************************88*/


// Structures
typedef struct _callbackinfo 
{
    double dblSampleTime;
    long lBufferSize;
    BYTE *pBuffer;
    BITMAPINFOHEADER bih;
} CALLBACKINFO;

CALLBACKINFO _g_cbi;
DWORD g_dwGraphRegister=0;  // For running object table
BOOL g_bOneShot = FALSE;
HANDLE g_CallBack;

// Note: this object is a SEMI-COM object, and can only be created statically.
// We use this little semi-com object to handle the sample-grab-callback,
// since the callback must provide a COM interface. We could have had an interface
// where you provided a function-call callback, but that's really messy, so we
// did it this way. You can put anything you want into this C++ object, even
// a pointer to a CDialog. Be aware of multi-thread issues though.
//

class CSampleGrabberCB : public ISampleGrabberCB 
{
public:
    // these will get set by the main thread below. We need to
    // know this in order to write out the bmp
	HWND m_hWnd;

    long lWidth;
    long lHeight;
    TCHAR m_szCapDir[MAX_PATH]; // the directory we want to capture to
    TCHAR m_szSnappedName[MAX_PATH];
    BOOL bFileWritten;

	BOOL m_bImg;

    CSampleGrabberCB( )
    {
		m_bImg = FALSE;
        m_szCapDir[0] = 0;
        bFileWritten = FALSE;
    }   

    // fake out any COM ref counting
    //
    STDMETHODIMP_(ULONG) AddRef() { return 2; }
    STDMETHODIMP_(ULONG) Release() { return 1; }

    // fake out any COM QI'ing
    //
    STDMETHODIMP QueryInterface(REFIID riid, void ** ppv)
    {
        if( riid == IID_ISampleGrabberCB || riid == IID_IUnknown ) 
        {
            *ppv = (void *) static_cast<ISampleGrabberCB*> ( this );
            return NOERROR;
        }    
        return E_NOINTERFACE;
    }

    // we don't implement this interface for this example
    //
    STDMETHODIMP SampleCB( double SampleTime, IMediaSample * pSample )
    {
        return 0;
    }

    // The sample grabber is calling us back on its deliver thread.
    // This is NOT the main app thread!
    //
    //           !!!!! WARNING WARNING WARNING !!!!!
    //
    // On Windows 9x systems, you are not allowed to call most of the 
    // Windows API functions in this callback.  Why not?  Because the
    // video renderer might hold the global Win16 lock so that the video
    // surface can be locked while you copy its data.  This is not an
    // issue on Windows 2000, but is a limitation on Win95,98,98SE, and ME.
    // Calling a 16-bit legacy function could lock the system, because 
    // it would wait forever for the Win16 lock, which would be forever
    // held by the video renderer.
    //
    // As a workaround, copy the bitmap data during the callback,
    // post a message to our app, and write the data later.
    //
    STDMETHODIMP BufferCB( double dblSampleTime, BYTE * pBuffer, long lBufferSize )
    {
        // this flag will get set to true in order to take a picture
        //
        if( !g_bOneShot )
            return 0;
		
//		g_bOneShot = FALSE;
		m_bImg = FALSE;

        // Since we can't access Windows API functions in this callback, just
        // copy the bitmap data to a global structure for later reference.
		if(_g_cbi.dblSampleTime == dblSampleTime) return 0;
		_g_cbi.dblSampleTime = dblSampleTime;
		_g_cbi.lBufferSize   = lBufferSize;

        // If we haven't yet allocated the data buffer, do it now.
        // Just allocate what we need to store the new bitmap.
		if (!_g_cbi.pBuffer)
			_g_cbi.pBuffer = new BYTE[lBufferSize];

		captureIndex++;
		if(captureIndex >= ULONG_MAX)
			captureIndex = 0;

		// Copy the bitmap data into our global buffer
		WaitForSingleObject(g_CallBack,INFINITE);
		ResetEvent(g_CallBack);
		if (_g_cbi.pBuffer)
			memcpy(_g_cbi.pBuffer, pBuffer, lBufferSize);
//			CopyMemory(_g_cbi.pBuffer, pBuffer, lBufferSize);
		SetEvent(g_CallBack);

        // Post a message to our application, telling it to come back
        // and write the saved data to a bitmap file on the user's disk.
		m_bImg = TRUE;
//SIJ        PostMessage(m_hWnd, WM_CAPTURE_BITMAP, 0, 0L);
		
        return 0;
    }

    // This is the implementation function that writes the captured video
    // data onto a bitmap on the user's disk.
    //
    BOOL CopyBitmap( )
    {
        BITMAPINFOHEADER bih;
        memset( &bih, 0, sizeof( bih ) );
        bih.biSize = sizeof( bih );
        bih.biWidth = lWidth;
        bih.biHeight = lHeight;
        bih.biPlanes = 1;
        bih.biBitCount = 24;
		bih.biCompression = BI_RGB;
		bih.biXPelsPerMeter= 0;
		bih.biYPelsPerMeter= 0;
		bih.biSizeImage    = _g_cbi.lBufferSize;
		bih.biClrUsed      = 0;
		bih.biClrImportant = 0;

        // Save bitmap header for later use when repainting the window
        memcpy(&(_g_cbi.bih), &bih, sizeof(bih));        

        return 0;
    }

};

CSampleGrabberCB g_CB;

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CVideoCap::CVideoCap()
{
	m_bShowWindow = OAFALSE;
	g_CallBack = CreateEvent(NULL,TRUE,TRUE,NULL);
    memset( &m_bih, 0, sizeof( BITMAPINFOHEADER ) );
	m_bCapturing = FALSE;
	captureIndex = 0;
	prev_CapIdx = ULONG_MAX;
}

CVideoCap::~CVideoCap()
{
	if(m_bCapturing){
		#ifdef REGISTER_FILTERGRAPH
		if (g_dwGraphRegister)
		{
			RemoveGraphFromRot(g_dwGraphRegister);
			g_dwGraphRegister = 0;
		}
		#endif

		SAFE_RELEASE(m_pMC);
//		SAFE_RELEASE(m_pXBar);
//		SAFE_RELEASE(m_pVAP);
		SAFE_RELEASE(m_pISGrabber);
	}
    CoUninitialize();
}

int CVideoCap::Create(HWND hwnd, BOOL b_ShowWindow)
{
    if(FAILED(CoInitialize(NULL)))
        return -1;
	m_hWnd = hwnd;
	g_CB.m_hWnd = m_hWnd;
	if(b_ShowWindow == FALSE) 
		m_bShowWindow = OAFALSE;
	else 
		m_bShowWindow = OATRUE;

	return 0L;
}

int CVideoCap::OpenVideo(int width, int height, int videoinput, int brightness, int contrast)
{
    HRESULT hr;
	captureIndex = 0;

	m_pBuilder = NULL;

    // create a sample grabber
    //
	IBaseFilter *pGrabBase = NULL;
	hr = CoCreateInstance(CLSID_SampleGrabber, NULL, CLSCTX_INPROC_SERVER,
		IID_IBaseFilter, reinterpret_cast<void**>(&pGrabBase));

	hr = pGrabBase->QueryInterface(IID_ISampleGrabber,
		reinterpret_cast<void**>(&m_pISGrabber));

    if( !m_pISGrabber )
    {
//        Error( TEXT("Could not create SampleGrabber (is qedit.dll registered?)"));
        return hr;
    }

    // Get whatever capture device exists
    //
    IBaseFilter *pSrc=NULL;

    // Use the system device enumerator and class enumerator to find
    // a video capture/preview device, such as a desktop USB video camera.
    hr = FindCaptureDevice(&pSrc);
    if (FAILED(hr))
    {
        // Don't display a message because FindCaptureDevice will handle it
        return hr;
    }

	// create a filter graph
    //
    hr = CoCreateInstance(CLSID_FilterGraph, NULL, CLSCTX_INPROC,
			IID_IGraphBuilder, (void **)&m_pGraph);
    if( !m_pGraph )
    {
//        Error( TEXT("Could not create filter graph") );
        return E_FAIL;
    }

	if (SUCCEEDED(hr))
	{
		// Create the Capture Graph Builder.
		hr = CoCreateInstance(CLSID_CaptureGraphBuilder2, NULL,
			CLSCTX_INPROC_SERVER, IID_ICaptureGraphBuilder2, 
			(void **)&m_pBuilder);
		if (SUCCEEDED(hr))
		{
			m_pBuilder->SetFiltergraph(m_pGraph);
		}
	};

	hr = m_pGraph->AddFilter(pSrc, L"Source");
	hr = m_pGraph->AddFilter(pGrabBase, L"Grabber");


    // force it to connect to video, 24 bit
    //
    CMediaType VideoType;
    VideoType.SetType( &MEDIATYPE_Video );
//    VideoType.SetSubtype( &MEDIASUBTYPE_MJPG );
    VideoType.SetSubtype( &MEDIASUBTYPE_RGB24 );
    hr = m_pISGrabber->SetMediaType( &VideoType ); // shouldn't fail
    if( FAILED( hr ) )
    {
//        Error( TEXT("Could not set media type"));
        return hr;
    }

    // !!! What if this interface isn't supported?
    // we use this interface to set the frame rate and get the capture size
    hr = m_pBuilder->FindInterface(&PIN_CATEGORY_CAPTURE,
                                      &MEDIATYPE_Interleaved,
                                      pSrc, IID_IAMStreamConfig, (void **)&m_pVSC);

    if(hr != NOERROR)
    {
        hr = m_pBuilder->FindInterface(&PIN_CATEGORY_CAPTURE,
                                          &MEDIATYPE_Video, pSrc,
                                          IID_IAMStreamConfig, (void **)&m_pVSC);
        if(hr != NOERROR)
        {
            // this means we can't set frame rate (non-DV only)
            return hr;
        }
    }

	//컬러 포맷을 24비트로 설정
	CComPtr<IPin> pPin;
    pSrc->FindPin(L"0", &pPin);   
	CComQIPtr<IAMStreamConfig> pSC(pPin);

	AM_MEDIA_TYPE *mediatype = NULL;
	m_pVSC->GetFormat(&mediatype);
	VIDEOINFOHEADER *pvi = (VIDEOINFOHEADER *)mediatype->pbFormat;
	int bmCnt = pvi->bmiHeader.biBitCount;
	
#ifndef _DEBUG
	mediatype->subtype = MEDIASUBTYPE_RGB24;
#else
	mediatype->subtype = MEDIASUBTYPE_MJPG;
#endif

	mediatype->lSampleSize  = height * width * bmCnt;
	pvi->bmiHeader.biHeight = height;
	pvi->bmiHeader.biWidth  = width;
	pvi->bmiHeader.biSizeImage = height * width * bmCnt;
	pvi->AvgTimePerFrame = 10000000 / 30;
	hr  = m_pVSC->SetFormat(mediatype);
	m_pVSC->GetFormat(&mediatype);

    // find the two pins and connect them
    //
    IPin * pSrcPin = GetOutPin( pSrc, 0 );
    IPin * pGrabPin = GetInPin( pGrabBase, 0 );
    hr = m_pGraph->Connect( pSrcPin, pGrabPin );
    if( FAILED( hr ) )
    {
//        Error( TEXT("Could not connect capture pin #0 to grabber.\r\n")
//               TEXT("Is the capture device being used by another application?"));
        return hr;
    }


    // ask for the connection media type so we know how big
    // it is, so we can write out bitmaps
    //
    AM_MEDIA_TYPE mt;
    hr = m_pISGrabber->GetConnectedMediaType( &mt );
    if ( FAILED( hr) )
    {
//        Error( TEXT("Could not read the connected media type"));
        return hr;
    }
    
    VIDEOINFOHEADER * vih = (VIDEOINFOHEADER*) mt.pbFormat;
    g_CB.lWidth  = vih->bmiHeader.biWidth;
    g_CB.lHeight = vih->bmiHeader.biHeight;

    m_bih.biSize = vih->bmiHeader.biSize;
    m_bih.biWidth = vih->bmiHeader.biWidth;
    m_bih.biHeight = vih->bmiHeader.biHeight;
    m_bih.biPlanes = vih->bmiHeader.biPlanes;
    m_bih.biBitCount = vih->bmiHeader.biBitCount;
	m_bih.biCompression = BI_RGB;
	m_bih.biXPelsPerMeter= 0;
	m_bih.biYPelsPerMeter= 0;
	m_bih.biSizeImage    = vih->bmiHeader.biSizeImage;
	m_bih.biClrUsed      = 0;
	m_bih.biClrImportant = 0;

    FreeMediaType( mt );

    // don't buffer the samples as they pass through
    //
//SIJTMP    m_pISGrabber->SetBufferSamples( FALSE );

    // only grab one at a time, stop stream after
    // grabbing one sample
    //
//SIJTMP	m_pISGrabber->SetOneShot( FALSE );

    // set the callback, so we can grab the one sample
    //
//SIJTMP    m_pISGrabber->SetCallback( &g_CB, 1 );

    // find the video window and stuff it in our window
    //
    CComQIPtr< IVideoWindow, &IID_IVideoWindow > pWindow = m_pGraph;
    if( !pWindow )
    {
//        Error( TEXT("Could not get video window interface"));
        return E_FAIL;
    }

    // set up the preview window to be in our dialog
    // instead of floating popup
    //
    HWND hwndPreview = m_hWnd;
    RECT rc;
    ::GetWindowRect( hwndPreview, &rc );
    pWindow->put_Owner( (OAHWND) hwndPreview );
	pWindow->put_AutoShow(OAFALSE);
    pWindow->put_Left( 0 );
    pWindow->put_Top( 0 );
    pWindow->put_Width( height );
    pWindow->put_Height( width );
    pWindow->put_Visible( m_bShowWindow );
    pWindow->put_WindowStyle( WS_CHILD | WS_CLIPSIBLINGS );

    // Add our graph to the running object table, which will allow
    // the GraphEdit application to "spy" on our graph
#ifdef REGISTER_FILTERGRAPH
    hr = AddGraphToRot(m_pGraph, &g_dwGraphRegister);
    if (FAILED(hr))
    {
//        Error(TEXT("Failed to register filter graph with ROT!"));
        g_dwGraphRegister = 0;
    }
#endif

	// Add Capture filter to our graph.
    hr = m_pGraph->AddFilter(pSrc, L"Video Capture");
    if (FAILED(hr))
    {
        pSrc->Release();
        return hr;
    }
	//캡쳐 디바이스 작동을 위한 필터
	m_pGraph->QueryInterface(IID_IMediaControl,   (void **)&m_pMC);

//SIJTMP	hr = m_pMC->Run();
	if( FAILED( hr ) )
	{
//			        Error( TEXT("Could not run graph"));
		return hr;
	}
	g_bOneShot = TRUE;

	//SAFE_RELEASE(pBuilder);
	SAFE_RELEASE(pGrabBase);
	pSrc->Release();
	m_bCapturing = TRUE;

	m_width    = width;
	m_height   = height;
	m_srcWidth = m_bih.biWidth;

    return 0;
}


int CVideoCap::CloseVideo()
{
//    HRESULT hr;

    // Stop media playback
//    if(m_pMC)
//        hr = m_pMC->Stop();

    // Clear global flags

    // Free DirectShow interfaces
    CloseInterfaces();

    // No current media state
	m_bCapturing = FALSE;

	return 0;
}

void CVideoCap::CloseInterfaces()
{
#ifdef REGISTER_FILTERGRAPH
    if (g_dwGraphRegister)
    {
        RemoveGraphFromRot(g_dwGraphRegister);
        g_dwGraphRegister = 0;
    }
#endif

    SAFE_RELEASE(m_pMC);
//SIJTMP	SAFE_RELEASE(m_pXBar);
//SIJTMP	SAFE_RELEASE(m_pVAP);
	SAFE_RELEASE(m_pVSC);
    SAFE_RELEASE(m_pGraph);
	SAFE_RELEASE(m_pISGrabber);
}

BOOL CVideoCap::GetFrame(unsigned char *pDIBImage,  double *captime)
{
	g_bOneShot = TRUE;

	if(prev_CapIdx == captureIndex) return FALSE;
	if(!_g_cbi.pBuffer) return FALSE;
	prev_CapIdx = captureIndex;

    // Save bitmap header for later use when repainting the window

	WaitForSingleObject(g_CallBack,INFINITE);
	ResetEvent(g_CallBack);

	//2016.10.14 memcpy(pDIBImage, _g_cbi.pBuffer, m_height * m_width * 3);
	//영상을 뒤집는다.
	int posBuf = m_height *  m_width * 3;
	for(int y=0; y < m_height; y++)
	{
			memcpy(&pDIBImage[y * m_width * 3], &_g_cbi.pBuffer[posBuf], m_width * 3);
			posBuf -= m_width * 3;
	}

	*captime = _g_cbi.dblSampleTime;
	SetEvent(g_CallBack);

	return TRUE;
}


BOOL CVideoCap::GetFrame720(unsigned char *pDIBImage,  double *captime)
{
	g_bOneShot = TRUE;

	if(prev_CapIdx == captureIndex) return FALSE;
	if(!_g_cbi.pBuffer) return FALSE;
	prev_CapIdx = captureIndex;

    // Save bitmap header for later use when repainting the window

	WaitForSingleObject(g_CallBack,INFINITE);
	ResetEvent(g_CallBack);

	//2016.10.14 memcpy(pDIBImage, _g_cbi.pBuffer, m_height * m_width * 3);
	for(int y = 0; y < 240; y++)
		for(int x = 0; x < 320; x++)
		{
			pDIBImage[3*(x+y*320)]   = BYTE((_g_cbi.pBuffer[3*((x+10)*2+y*720*2)    ] + _g_cbi.pBuffer[3*((x+10)*2+y*720*2) + 3]) / 2);
			pDIBImage[3*(x+y*320)+1] = BYTE((_g_cbi.pBuffer[3*((x+10)*2+y*720*2) + 1] + _g_cbi.pBuffer[3*((x+10)*2+y*720*2) + 4]) / 2);
			pDIBImage[3*(x+y*320)+2] = BYTE((_g_cbi.pBuffer[3*((x+10)*2+y*720*2) + 2] + _g_cbi.pBuffer[3*((x+10)*2+y*720*2) + 5]) / 2);
		}

	*captime = _g_cbi.dblSampleTime;
	SetEvent(g_CallBack);

	return TRUE;
}


BOOL CVideoCap::GetFrame640(unsigned char *pDIBImage,  double *captime)
{
	g_bOneShot = TRUE;

	if(prev_CapIdx == captureIndex) return FALSE;
	if(!_g_cbi.pBuffer) return FALSE;
	prev_CapIdx = captureIndex;

    // Save bitmap header for later use when repainting the window

	WaitForSingleObject(g_CallBack,INFINITE);
	ResetEvent(g_CallBack);

	//2016.10.14 memcpy(pDIBImage, _g_cbi.pBuffer, m_height * m_width * 3);
	for(int y = 0; y < 240; y++)
		for(int x = 0; x < 320; x++)
		{
			pDIBImage[3*(x+y*320)]   = BYTE((_g_cbi.pBuffer[3*(x*2+y*640*2)    ] + _g_cbi.pBuffer[3*(x*2+y*640*2) + 3]) / 2);
			pDIBImage[3*(x+y*320)+1] = BYTE((_g_cbi.pBuffer[3*(x*2+y*640*2) + 1] + _g_cbi.pBuffer[3*(x*2+y*640*2) + 4]) / 2);
			pDIBImage[3*(x+y*320)+2] = BYTE((_g_cbi.pBuffer[3*(x*2+y*640*2) + 2] + _g_cbi.pBuffer[3*(x*2+y*640*2) + 5]) / 2);
		}

	*captime = _g_cbi.dblSampleTime;
	SetEvent(g_CallBack);

	return TRUE;
}



BOOL CVideoCap::GetFrame256(unsigned char *pDIBImage,  double *captime)
{
	g_bOneShot = TRUE;
	double ftemp;

	if(prev_CapIdx == captureIndex) return FALSE;
	if(!_g_cbi.pBuffer) return FALSE;
	prev_CapIdx = captureIndex;

    // Save bitmap header for later use when repainting the window
//	for (m_SrcIndex=0, m_DestIndex=0; m_SrcIndex <_g_cbi.lBufferSize-1 ; m_SrcIndex+=3, m_DestIndex++)
	m_SrcIndex=0;
	m_DestIndex=0;

	WaitForSingleObject(g_CallBack,INFINITE);
	ResetEvent(g_CallBack);
	for(int i=m_height-1; i>=0; i--)
	{
		m_SrcIndex = i * m_width * 3;
		for(int j=0; j<m_width; j++)
		{
			ftemp = (_g_cbi.pBuffer[m_SrcIndex] + _g_cbi.pBuffer[m_SrcIndex+1] + _g_cbi.pBuffer[m_SrcIndex+2]) / 3;
			pDIBImage[m_DestIndex] =unsigned char(ftemp + 0.5);
			m_SrcIndex+=3;
			m_DestIndex++;
		}
	}
	*captime = _g_cbi.dblSampleTime;
	SetEvent(g_CallBack);

	return TRUE;
}


HRESULT CVideoCap::Pause()
{
	m_pMC->Pause();
	return 0;
}

HRESULT CVideoCap::GetPin(IBaseFilter *pFilter, PIN_DIRECTION PinDir, IPin **ppPin)
{
    IEnumPins  *pEnum;
    IPin       *pPin;
    pFilter->EnumPins(&pEnum);
    while(pEnum->Next(1, &pPin, 0) == S_OK)
    {
        PIN_DIRECTION PinDirThis;
        pPin->QueryDirection(&PinDirThis);
        if (PinDir == PinDirThis)
        {
            pEnum->Release();
            *ppPin = pPin;
            return S_OK;
        }
        pPin->Release();
    }
    pEnum->Release();
    return E_FAIL;  
}


HRESULT CVideoCap::Play()
{
	return m_pMC->Run();
}

HRESULT CVideoCap::FindCaptureDevice(IBaseFilter ** ppSrcFilter)
{
    HRESULT hr;
    IBaseFilter * pSrc = NULL;
    CComPtr <IMoniker> pMoniker =NULL;
    ULONG cFetched;
   
    // Create the system device enumerator
    CComPtr <ICreateDevEnum> pDevEnum =NULL;

    hr = CoCreateInstance (CLSID_SystemDeviceEnum, NULL, CLSCTX_INPROC,
        IID_ICreateDevEnum, (void ** ) &pDevEnum);
    if (FAILED(hr))
    {
//        Msg(TEXT("Couldn't create system enumerator!  hr=0x%x"), hr);
        return hr;
    }

    // Create an enumerator for the video capture devices
    CComPtr <IEnumMoniker> pClassEnum = NULL;

    hr = pDevEnum->CreateClassEnumerator (CLSID_VideoInputDeviceCategory, &pClassEnum, 0);
    if (FAILED(hr))
    {
//        Msg(TEXT("Couldn't create class enumerator!  hr=0x%x"), hr);
        return hr;
    }

    // If there are no enumerators for the requested type, then 
    // CreateClassEnumerator will succeed, but pClassEnum will be NULL.
    if (pClassEnum == NULL)
    {
//        MessageBox(ghApp,TEXT("No video capture device was detected.\r\n\r\n")
  //                 TEXT("This sample requires a video capture device, such as a USB WebCam,\r\n")
    //               TEXT("to be installed and working properly.  The sample will now close."),
      //             TEXT("No Video Capture Hardware"), MB_OK | MB_ICONINFORMATION);
        return E_FAIL;
    }

    // Use the first video capture device on the device list.
    // Note that if the Next() call succeeds but there are no monikers,
    // it will return S_FALSE (which is not a failure).  Therefore, we
    // check that the return code is S_OK instead of using SUCCEEDED() macro.
    if (S_OK == (pClassEnum->Next (1, &pMoniker, &cFetched)))
    {
        // Bind Moniker to a filter object
        hr = pMoniker->BindToObject(0,0,IID_IBaseFilter, (void**)&pSrc);
        if (FAILED(hr))
        {
//            Msg(TEXT("Couldn't bind moniker to filter object!  hr=0x%x"), hr);
            return hr;
        }
    }
    else
    {
//        Msg(TEXT("Unable to access video capture device!"));   
        return E_FAIL;
    }

    // Copy the found filter pointer to the output parameter.
    // Do NOT Release() the reference, since it will still be used
    // by the calling function.
    *ppSrcFilter = pSrc;

    return hr;
}


HRESULT CVideoCap::SetInput(int pinIndex)
{

    HRESULT hr;
	hr = m_pXBar->Route(0,pinIndex);
	return hr;
}

IPin * CVideoCap::GetInPin( IBaseFilter * pFilter, int Num )
{
    CComPtr< IPin > pComPin;
//    GetPin(pFilter, PINDIR_INPUT, Num, &pComPin);
	GetPin(pFilter, PINDIR_INPUT, &pComPin);
    return pComPin;
}


IPin * CVideoCap::GetOutPin( IBaseFilter * pFilter, int Num )
{
    CComPtr< IPin > pComPin;
//    GetPin(pFilter, PINDIR_OUTPUT, Num, &pComPin);
    GetPin(pFilter, PINDIR_OUTPUT, &pComPin);
    return pComPin;
}


