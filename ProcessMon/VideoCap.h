// DirectVideoFrame.h: interface for the CDirectVideoFrame class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_DIRECTVIDEOFRAME_H__B32E0EE1_5B5B_11D6_8410_005004C0D66C__INCLUDED_)
#define AFX_DIRECTVIDEOFRAME_H__B32E0EE1_5B5B_11D6_8410_005004C0D66C__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "stdafx.h"
#include <dshow.h>

#pragma include_alias( "dxtrans.h", "qedit.h" )

#define __IDxtCompositor_INTERFACE_DEFINED__

#define __IDxtAlphaSetter_INTERFACE_DEFINED__

#define __IDxtJpeg_INTERFACE_DEFINED__

#define __IDxtKey_INTERFACE_DEFINED__

#include "Qedit.h"

#include <strmif.h>
#include <atlbase.h>

HRESULT AddGraphToRot(IUnknown *pUnkGraph, DWORD *pdwRegister);
void RemoveGraphFromRot(DWORD pdwRegister);

// Constants
#define WM_CAPTURE_BITMAP   WM_USER + 500
#define WM_GRAPHNOTIFY  WM_USER+13
#define SAFE_RELEASE(x) { if (x) x->Release(); x = NULL; }

extern unsigned long captureIndex;



/***********************************************************88*/
//				CDirectVideoFrame
/***********************************************************88*/
class CVideoCap
{
//    friend class CSampleGrabberCB;

public:
	int m_SrcIndex, m_DestIndex;
	int m_width, m_height;
	int m_srcWidth;
	int prev_CapIdx;
	HRESULT SetInput(int pinIndex);
	HRESULT FindCaptureDevice(IBaseFilter ** ppSrcFilter);
	HRESULT Play();
	HRESULT Pause();//일시정지(단 일시정지 중에는 모든 프레임 접근 명령은 에러발생!!!!)
	BOOL GetFrame(unsigned char *pDIBImage,  double *captime);// 현재 프래임 읽기
	BOOL GetFrame720(unsigned char *pDIBImage,  double *captime);// 현재 프래임 읽기
	BOOL GetFrame640(unsigned char *pDIBImage,  double *captime);// 현재 프래임 읽기
	BOOL GetFrame256(unsigned char *pDIBImage,  double *captime);// 현재 프래임 읽기
	int CloseVideo();//비디오 파일 닫기 : 핸들사용 불능 상태
	int OpenVideo(int width, int height, int videoinput, int brightness, int contrast);//비디오 파일 닫기 : 핸들얻기
	int Create(HWND hwnd, BOOL b_ShowWindow);//COM의 초기화및 윈도우 연결
	CVideoCap();//
	virtual ~CVideoCap();

protected:
	BITMAPINFOHEADER m_bih;
	int m_bCapturing;

	// DirectShow interfaces
    IGraphBuilder  *m_pGraph;
	ISampleGrabber *m_pISGrabber;
	ICaptureGraphBuilder2 *m_pBuilder;
	IAMStreamConfig *m_pVSC;


	HWND m_hWnd;

	//비디오 파일 오픈 시 초기화
	IMediaControl	*m_pMC;
	IAMCrossbar     *m_pXBar;
	IAMVideoProcAmp *m_pVAP;


	long m_bShowWindow;

	HRESULT GetPin(IBaseFilter *pFilter, PIN_DIRECTION PinDir, IPin **ppPin);
	void CloseInterfaces();
	IPin * GetInPin( IBaseFilter * pFilter, int Num );
	IPin * GetOutPin( IBaseFilter * pFilter, int Num );

};


#endif // !defined(AFX_DIRECTVIDEOFRAME_H__B32E0EE1_5B5B_11D6_8410_005004C0D66C__INCLUDED_)
